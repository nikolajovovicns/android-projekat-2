package com.example.androidprojekat2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class DodajStudenta extends AppCompatActivity {

    private EditText editTextIme, editTextPrezime, editTextBrojIndeksa;
    private Spinner spinnerSmer, spinnerPredmet;
    private Button buttonDodajStudenta, buttonPogledajStudente;
    private Baza baza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_studenta);

        editTextIme = findViewById(R.id.editTextIme);
        editTextPrezime = findViewById(R.id.editTextPrezime);
        editTextBrojIndeksa = findViewById(R.id.editTextBrojIndeksa);
        spinnerSmer = findViewById(R.id.spinnerSmer);
        spinnerPredmet = findViewById(R.id.spinnerPredmet);
        buttonDodajStudenta = findViewById(R.id.buttonDodajStudenta);
        buttonPogledajStudente = findViewById(R.id.buttonPogledajStudente);

        baza = new Baza(this);

        buttonDodajStudenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ime = editTextIme.getText().toString();
                String prezime = editTextPrezime.getText().toString();
                String brojIndeksa = editTextBrojIndeksa.getText().toString();
                String smer = spinnerSmer.getSelectedItem().toString();
                String predmet = spinnerPredmet.getSelectedItem().toString();

                if (ime.isEmpty() || prezime.isEmpty() || brojIndeksa.isEmpty()) {
                    Toast.makeText(DodajStudenta.this, "Polja ne mogu biti prazna !", Toast.LENGTH_SHORT).show();
                    return;
                }

                baza.dodajStudenta(ime, prezime, brojIndeksa, smer, predmet);
                Toast.makeText(DodajStudenta.this, "Uspešno !", Toast.LENGTH_SHORT).show();
            }
        });

        buttonPogledajStudente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DodajStudenta.this, PrikazStudenta.class);
                startActivity(intent);
            }
        });
    }
}