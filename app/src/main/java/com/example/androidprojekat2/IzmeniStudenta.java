package com.example.androidprojekat2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

public class IzmeniStudenta extends AppCompatActivity {

    private EditText etIme, etPrezime, etBrojIndeksa;
    private Spinner spSmerIzmeni,spPredmetIzmeni;
    private MaterialButton btnDodaj, btnPregled, btnSacuvaj;

    private Baza baza;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izmeni_studenta);

        etIme = findViewById(R.id.et_ime);
        etPrezime = findViewById(R.id.et_prezime);
        etBrojIndeksa = findViewById(R.id.et_broj_indeksa);

        spSmerIzmeni = findViewById(R.id.sp_smer_izmeni);
        spPredmetIzmeni = findViewById(R.id.sp_predmet_izmeni);

        btnDodaj = findViewById(R.id.btn_dodajIzmena);
        btnPregled = findViewById(R.id.btn_pregledIzmena);
        btnSacuvaj = findViewById(R.id.btn_sacuvajIzmena);

        baza = new Baza(this);

        Intent intent = getIntent();
        if (intent != null) {
            int studentId = intent.getIntExtra("studentId", -1);
            String studentIme = intent.getStringExtra("studentIme");
            String studentPrezime = intent.getStringExtra("studentPrezime");
            String studentBrojIndeksa = intent.getStringExtra("studentBrojIndeksa");

            etIme.setText(studentIme);
            etPrezime.setText(studentPrezime);
            etBrojIndeksa.setText(studentBrojIndeksa);
        }

        btnDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IzmeniStudenta.this, DodajStudenta.class);
                startActivity(intent);
            }
        });

        btnPregled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IzmeniStudenta.this, PrikazStudenta.class);
                startActivity(intent);
            }
        });

        btnSacuvaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ime = etIme.getText().toString();
                String prezime = etPrezime.getText().toString();
                String brojIndeksa = etBrojIndeksa.getText().toString();
                String smer = spSmerIzmeni.getSelectedItem().toString();
                String predmet = spPredmetIzmeni.getSelectedItem().toString();

                if (ime.isEmpty() || prezime.isEmpty() || brojIndeksa.isEmpty()) {
                    Toast.makeText(IzmeniStudenta.this, "Polja ne mogu biti prazna !", Toast.LENGTH_SHORT).show();
                    return;
                }

                int studentId = getIntent().getIntExtra("studentId", -1);
                baza.izmeniStudenta(studentId, ime, prezime, brojIndeksa, smer, predmet);

                Intent intent = new Intent(IzmeniStudenta.this, PrikazStudenta.class);
                startActivity(intent);
            }
        });
    }
}