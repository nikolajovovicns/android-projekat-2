package com.example.androidprojekat2;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;

import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {

    private List<Student> studenti;
    private Baza baza;

    public StudentAdapter(List<Student> studenti, Context context) {
        this.studenti = studenti;
        baza = new Baza(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Student student = studenti.get(position);
        holder.tvIme.setText(student.getIme());
        holder.tvPrezime.setText(student.getPrezime());
        holder.tvBrojIndeksa.setText(student.getBrojIndeksa());
        holder.tvSmer.setText(student.getSmer());
        holder.tvPredmet.setText(student.getPredmet());
    }

    @Override
    public int getItemCount() {
        return studenti.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvIme;
        public TextView tvPrezime;
        public TextView tvBrojIndeksa;
        public TextView tvSmer;
        public TextView tvPredmet;

        public MaterialButton btnDelete;
        public MaterialButton btnIzmeni;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvIme = itemView.findViewById(R.id.tv_ime);
            tvPrezime = itemView.findViewById(R.id.tv_prezime);
            tvBrojIndeksa = itemView.findViewById(R.id.tv_broj_indeksa);
            tvSmer = itemView.findViewById(R.id.tv_smer);
            tvPredmet = itemView.findViewById(R.id.tv_predmet);

            btnDelete = itemView.findViewById(R.id.btn_izbrisi);
            btnIzmeni = itemView.findViewById(R.id.btn_izmeni);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        if (baza != null) {
                            Student student = studenti.get(position);
                            if (student != null) {
                                baza.obrisiStudenta(student.getId());
                                studenti.remove(position);
                                notifyItemRemoved(position);
                                Log.d("StudentAdapter", "Student deleted at position " + position);
                                Log.d("StudentAdapter", "Student id " + student.getId());
                            }
                            Log.d("StudentAdapter", "Stanje studenta je " + student);
                        }
                        Log.d("StudentAdapter", "Stanje baze je " + baza);
                    }
                }
            });
            btnIzmeni.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        Student student = studenti.get(position);
                        Intent intent = new Intent(v.getContext(), IzmeniStudenta.class);
                        intent.putExtra("studentId", student.getId());
                        intent.putExtra("studentIme", student.getIme());
                        intent.putExtra("studentPrezime", student.getPrezime());
                        intent.putExtra("studentBrojIndeksa", student.getBrojIndeksa());
                        intent.putExtra("studentSmer", student.getSmer());
                        intent.putExtra("studentPredmet", student.getPredmet());
                        v.getContext().startActivity(intent);
                    }
                }
            });
        }
    }
}