package com.example.androidprojekat2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;

import java.util.ArrayList;

public class PrikazStudenta extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Student> studenti;
    private Baza baza;
    private StudentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prikaz_studenta);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        studenti = new ArrayList<>();
        baza = new Baza(this);

        prikaziStudente();
    }

    @SuppressLint("Range")
    private void prikaziStudente() {
        Cursor cursor = baza.getReadableDatabase().query(baza.getTableName(),
                null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            Student student = new Student();
            student.setId(cursor.getInt(cursor.getColumnIndex(baza.getColumnId())));
            student.setIme(cursor.getString(cursor.getColumnIndex(baza.getColumnIme())));
            student.setPrezime(cursor.getString(cursor.getColumnIndex(baza.getColumnPrezime())));
            student.setBrojIndeksa(cursor.getString(cursor.getColumnIndex(baza.getColumnBrojIndeksa())));
            student.setSmer(cursor.getString(cursor.getColumnIndex(baza.getColumnSmer())));
            student.setPredmet(cursor.getString(cursor.getColumnIndex(baza.getColumnPredmet())));
            studenti.add(student);
        }

        cursor.close();

        adapter = new StudentAdapter(studenti, PrikazStudenta.this);
        recyclerView.setAdapter(adapter);
    }
}