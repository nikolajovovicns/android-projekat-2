package com.example.androidprojekat2;

public class Student {
    private int id;
    private String ime;
    private String prezime;
    private String brojIndeksa;
    private String smer;
    private String predmet;

    public  Student(){};

    public Student(int id, String ime, String prezime, String brojIndeksa, String smer, String predmet) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.brojIndeksa = brojIndeksa;
        this.smer = smer;
        this.predmet = predmet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getBrojIndeksa() {
        return brojIndeksa;
    }

    public void setBrojIndeksa(String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }

    public String getSmer() {
        return smer;
    }

    public void setSmer(String smer) {
        this.smer = smer;
    }

    public String getPredmet() {
        return predmet;
    }

    public void setPredmet(String predmet) {
        this.predmet = predmet;
    }

    @Override
    public String toString() {
        return "Ime - " + ime + "\n" + "Prezime - " + prezime + "\n" + "Broj indeksa - " + brojIndeksa + "\n" + "Smer: " + smer + "\n" + "Predmet - " + predmet;
    }
}
