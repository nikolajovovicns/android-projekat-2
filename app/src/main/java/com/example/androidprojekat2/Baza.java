package com.example.androidprojekat2;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Baza extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "studenti.db";
    private static final String TABLE_NAME = "studenti";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_IME = "ime";
    private static final String COLUMN_PREZIME = "prezime";
    private static final String COLUMN_BROJ_INDEKSA = "index_number";
    private static final String COLUMN_SMER = "department";
    private static final String COLUMN_PREDMET = "subject";


    public String getTableName() {
        return TABLE_NAME;
    }

    public String getColumnId() {
        return COLUMN_ID;
    }

    public String getColumnIme() {
        return COLUMN_IME;
    }

    public String getColumnPrezime() {
        return COLUMN_PREZIME;
    }

    public String getColumnBrojIndeksa() {
        return COLUMN_BROJ_INDEKSA;
    }

    public String getColumnSmer() {
        return COLUMN_SMER;
    }

    public String getColumnPredmet() {
        return COLUMN_PREDMET;
    }

    public Baza(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_IME + " TEXT, " +
                COLUMN_PREZIME + " TEXT, " +
                COLUMN_BROJ_INDEKSA + " TEXT, " +
                COLUMN_SMER + " TEXT, " +
                COLUMN_PREDMET + " TEXT)";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean dodajStudenta(String ime, String prezime, String brojIndeksa, String smer, String predmet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IME, ime);
        contentValues.put(COLUMN_PREZIME, prezime);
        contentValues.put(COLUMN_BROJ_INDEKSA, brojIndeksa);
        contentValues.put(COLUMN_SMER, smer);
        contentValues.put(COLUMN_PREDMET, predmet);
        long result = db.insert(TABLE_NAME, null, contentValues);
        return result != -1;
    }

    public void obrisiStudenta(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + "=?", new String[]{String.valueOf(id)});
        db.close();
    }

    public void izmeniStudenta(int id, String ime, String prezime, String brojIndeksa, String smer, String predmet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IME, ime);
        contentValues.put(COLUMN_PREZIME, prezime);
        contentValues.put(COLUMN_BROJ_INDEKSA, brojIndeksa);
        contentValues.put(COLUMN_SMER, smer);
        contentValues.put(COLUMN_PREDMET, predmet);
        db.update(TABLE_NAME, contentValues, COLUMN_ID + "=?", new String[]{String.valueOf(id)});
        db.close();
    }
}
